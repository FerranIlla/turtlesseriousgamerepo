﻿using System.Collections;
using UnityEngine;

public class TargetSelectButton : MonoBehaviour {

    public GameObject characterTargetPrefab;

    public void SelectCharacterTarget()
    {
        GameObject.Find("BattleManager").GetComponent<BattleStateMachine>().Input2(characterTargetPrefab);
    }
}
