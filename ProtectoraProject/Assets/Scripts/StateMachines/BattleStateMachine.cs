﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleStateMachine : MonoBehaviour {
    
    public enum PerformAction
    {
        WAIT,//waiting your turn/ACTION
        TAKEACTION,//in your turn
        PERFORMACTION,//doned turn
        ENDACTION,
    }

    public PerformAction battleState;

    [HideInInspector]
    public GameObject currCharacter;//campio que hagi seleccionat en qualsevol moment el player, to manage
    public bool turnPlayer1;//player 1 begins the game;;

    public enum TurnGUI
    {
        ACTIVATE,
        WAITING,
        INPUT1,//ACTION
        INPUT2,//SELECTING
        INPUTMOVE,
        DONE
    }

    public TurnGUI turnInput;
    public HandleTurn turnChoise;

    //quantity of character in the player1 team
    public List<GameObject> T1CharactersInBattle = new List<GameObject>();
    private P1StateMachine P1SM;
    //quantity of character in the player2 team
    public List<GameObject> T2CharactersInBattle = new List<GameObject>();
    private P2StateMachine P2SM;

    private GameObject camera;
    [Space(10)]
    [Header("GUI Variables")]
    #region GUI
    public GameObject targetButton;
    public Transform Spacer;
    private bool ActivateActionPanel;
    public GameObject ActionPanel;
    public GameObject SelectTargetPanel;
    private GameObject newButton;
    public List<GameObject> listButtons = new List<GameObject>();
    public GameObject passTurnObject;
    public Text infoTurnPanel;
    public Text numTP1, numTP2;
    public int numOfTurn;
    #endregion GUI

    #region InstatiateListPlayers
    private GameObject turtleP1, turtleP2;
    void InstantiateP1Turtles()
    {
        turtleP1 = GameObject.FindGameObjectWithTag("CharacterP1");
        for (int i = 0; i < 3; i++)
        {
            GameObject p1Turtle = Instantiate(turtleP1, new Vector3(i * (-15)+15, 1.5f, 3), Quaternion.identity);
            p1Turtle.GetComponent<P1StateMachine>().enabled = true;
            p1Turtle.GetComponent<P1StateMachine>().currentTurtleP1.theName = p1Turtle.GetComponent<P1StateMachine>().currentTurtleP1.theName + ":Num " + (i + 1);
            p1Turtle.GetComponent<Rigidbody>().useGravity = true;
        }
        Destroy(turtleP1.gameObject);
    }

    void InstantiateP2Turtles()
    {
        turtleP2 = GameObject.FindGameObjectWithTag("CharacterP2");
        for (int i = 0; i < 3; i++)
        {
            GameObject p2Turtle = Instantiate(turtleP2, new Vector3(i *(-15)+15, 1.5f, -15), Quaternion.identity);
            p2Turtle.GetComponent<P2StateMachine>().enabled = true;
            p2Turtle.GetComponent<P2StateMachine>().currentTurtleP2.theName = p2Turtle.GetComponent<P2StateMachine>().currentTurtleP2.theName + ":Num " + (i + 1);
            p2Turtle.GetComponent<Rigidbody>().useGravity = true;
        }
        Destroy(turtleP2.gameObject);
    }

    #endregion InstantiateListPlayers

    private void Awake()
    {
        InstantiateP1Turtles();
        InstantiateP2Turtles();
    }
    void Start () {

        camera = GameObject.FindGameObjectWithTag("MainCamera");
        //Listas Players
        T1CharactersInBattle.AddRange(GameObject.FindGameObjectsWithTag("CharacterP1"));
        T2CharactersInBattle.AddRange(GameObject.FindGameObjectsWithTag("CharacterP2"));
        //radev parche per quan sinicialitza lescena
        T1CharactersInBattle.RemoveAt(0);
        T2CharactersInBattle.RemoveAt(0);

        //Variables inGame
        currCharacter = null;
        turnPlayer1 = true;
        numOfTurn = 1;
        battleState = PerformAction.WAIT;
        turnInput = TurnGUI.WAITING;

        //Variables GUI
        ActivateActionPanel = false;
        ActionPanel.SetActive(false);
        SelectTargetPanel.SetActive(false);
        passTurnObject.SetActive(false);
        infoTurnPanel.GetComponent<Text>().text = numOfTurn.ToString();
        numTP1.text = T1CharactersInBattle.Count.ToString();
        numTP2.text = T2CharactersInBattle.Count.ToString();

    }

	void Update () {
        switch (battleState)
        {
            case (PerformAction.WAIT):
                PressCharacter();//la propia funcio distingeix qui te el torn
                if (!turnPlayer1)
                {
                }
                break;
            case (PerformAction.TAKEACTION)://es dirigeix a fer una accio
                if (turnPlayer1)
                {
                    P1SM = currCharacter.GetComponent<P1StateMachine>();
                    turnInput = TurnGUI.ACTIVATE;
                    battleState = PerformAction.PERFORMACTION;
                }
                //turnPlayer2
                else
                {
                    P2SM = currCharacter.GetComponent<P2StateMachine>();
                    turnInput = TurnGUI.ACTIVATE;
                    battleState = PerformAction.PERFORMACTION;
                }
                break;
            case (PerformAction.PERFORMACTION):
                break;
            case (PerformAction.ENDACTION):
                checkPassTurn();
                break;
        }
        switch (turnInput)
        {
            case (TurnGUI.ACTIVATE):
                if (currCharacter != null && ActivateActionPanel)//and es fa un input a un dels personatges corresponents
                {
                    turnChoise = new HandleTurn();
                }
                break;
            case (TurnGUI.WAITING)://iddle o off
                break;
            case (TurnGUI.INPUT1):
              //  entra aquí quan es crida InputAttack() clican el boto atac
                break;
            case (TurnGUI.INPUT2):
                break;
            case (TurnGUI.INPUTMOVE):
                currCharacter.GetComponent<Movement>().enabled = true;
                break;
            case (TurnGUI.DONE):
                P1InputDone();
                ActivateActionPanel = false;
                ActionPanel.SetActive(false);
                SelectTargetPanel.SetActive(false);
                currCharacter = null;
                turnInput = TurnGUI.WAITING; 
                break;

        }

        WinCondition();
    }

    void WinCondition()
    {
        if(T1CharactersInBattle.Count == 0)
        {
            Debug.Log("Player2 ha guanyat");

        }
        else if(T2CharactersInBattle.Count == 0)
        {
            Debug.Log("Player1 ha guanyar");
        }
      //  SceneManager.LoadScene(0);
    }
    void CreateTargetsButtons()
    {
        if (turnPlayer1)
        {

            List<GameObject> targetInRange = P1SM.targetsInRangeAtk();
            foreach (GameObject currentTurtleP2 in targetInRange)
            {
                GameObject newButton = Instantiate(targetButton) as GameObject;
                TargetSelectButton button = newButton.GetComponent<TargetSelectButton>();
                P2StateMachine cur_characterT2 = currentTurtleP2.GetComponent<P2StateMachine>();

                Text buttonText = newButton.transform.Find("Text").gameObject.GetComponent<Text>(); ;
                buttonText.text = cur_characterT2.currentTurtleP2.theName;

                button.characterTargetPrefab = currentTurtleP2;

                newButton.transform.SetParent(Spacer, false);
                listButtons.Add(newButton);
            }
        }

        else
        {
            List<GameObject> targetInRange2 = P2SM.targetsInRangeAtk();
            foreach (GameObject currentTurtleP1 in targetInRange2)
            {
                GameObject newButton = Instantiate(targetButton) as GameObject;
                TargetSelectButton button = newButton.GetComponent<TargetSelectButton>();
                P1StateMachine cur_characterT1 = currentTurtleP1.GetComponent<P1StateMachine>();

                Text buttonText = newButton.transform.Find("Text").gameObject.GetComponent<Text>(); ;
                buttonText.text = cur_characterT1.currentTurtleP1.theName;

                button.characterTargetPrefab = currentTurtleP1;

                newButton.transform.SetParent(Spacer, false);
                listButtons.Add(newButton);
            }
        }


    }

    void PressCharacter()
    { 
        if (Input.GetMouseButtonUp(0))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (turnPlayer1)
            {
                if (Physics.Raycast(raycast, out hit))
                {

                    if (hit.collider.tag == "CharacterP1" && !ActivateActionPanel && hit.collider.GetComponent<P1StateMachine>().currentTurtleP1.hasAtck == false)
                    {
                        currCharacter = hit.collider.gameObject;
                        battleState = PerformAction.TAKEACTION;
                        ActivateActionPanel = true;
                        ActionPanel.SetActive(true);

                    }

                    if (hit.collider.tag == "CharacterP1" && !ActivateActionPanel && hit.collider.GetComponent<P1StateMachine>().currentTurtleP1.hasAtck == true)
                    {
                        Debug.Log("aquesst loki ja ha atacat");
                    }
                }
            }
            //!turnPlayer1
            else
            {
                if (Physics.Raycast(raycast, out hit))
                {

                    if (hit.collider.tag == "CharacterP2" && !ActivateActionPanel && hit.collider.GetComponent<P2StateMachine>().currentTurtleP2.hasAtck == false)
                    {
                        currCharacter = hit.transform.gameObject;
                        battleState = PerformAction.TAKEACTION;
                        ActivateActionPanel = true;
                        ActionPanel.SetActive(true);

                    }

                    if (hit.collider.tag == "CharacterP2" && !ActivateActionPanel && hit.collider.GetComponent<P2StateMachine>().currentTurtleP2.hasAtck == true)
                    {
                        Debug.Log("aquesst loki ja ha atacat");
                    }
                }
            }

        }
    }

    public void CloseGui()
    {
        if (ActivateActionPanel)
        {
            ActivateActionPanel = false;
            ActionPanel.SetActive(false);
         //   P1SelectTargetPanel.SetActive(false);
            battleState = PerformAction.WAIT;
        }    
        if (SelectTargetPanel)
        {
            foreach(GameObject newButton in listButtons)
            {
                Destroy(newButton.gameObject);
            }
            SelectTargetPanel.SetActive(false);
        }

        currCharacter = null;
        battleState = PerformAction.WAIT;
        turnInput = TurnGUI.WAITING;
    }

    void P1InputDone()
    {
       foreach(GameObject newButton in listButtons)//clean the list of buttons targets
        {
            Destroy(newButton.gameObject);
        }
    }

    public void InputAttack()
    {
        turnInput = TurnGUI.INPUT1;
        //turnChoise.Attacker = currCharacter.GetComponent<BaseTurtle>().theName;
        CreateTargetsButtons();
        if (turnPlayer1)
        {
            turnChoise.Attacker = currCharacter.GetComponent<P1StateMachine>().currentTurtleP1.theName;
        }
        else
        {
            turnChoise.Attacker = currCharacter.GetComponent<P2StateMachine>().currentTurtleP2.theName;
        }
        turnChoise.AttackersGameObject = currCharacter;
        turnChoise.team = currCharacter.tag;
        ActionPanel.SetActive(false);
        SelectTargetPanel.SetActive(true);
    }

    public void Input2(GameObject choosenTarget)//Target Selection per part de t1
    {
        turnChoise.TargetsGameObject = choosenTarget;
        // battleState = PerformAction.PERFORMACTION;
        if (turnPlayer1)
        {
            P1SM.currentState = P1StateMachine.TurnState.ACTION;
        }
        else
        {
            P2SM.currentState = P2StateMachine.TurnState.ACTION;
        }
        turnInput = TurnGUI.DONE;

    }

    private void checkPassTurn()
    {
  
        if (turnPlayer1)
        {
            for (int i = 0; i < T1CharactersInBattle.Count; i++)
            {
                if (!T1CharactersInBattle[i].GetComponent<P1StateMachine>().currentTurtleP1.hasAtck)
                {
                    Debug.Log("comprovan si puc pasar torn, no hauria");
                    battleState = PerformAction.WAIT;
                    return;
                }

                else if(i == (T1CharactersInBattle.Count - 1) && T1CharactersInBattle[i].GetComponent<P1StateMachine>().currentTurtleP1.hasAtck)
                { 
                    Debug.Log("comprovan si puc pasar torn, si hauria");
                    passTurnObject.SetActive(true);
                    battleState = PerformAction.WAIT;
                }
            }
        }

        else
        {
            for (int i = 0; i < T2CharactersInBattle.Count; i++)
            {
                if (!T2CharactersInBattle[i].GetComponent<P2StateMachine>().currentTurtleP2.hasAtck)
                {
                    Debug.Log("comprovan si puc pasar torn, no hauria");
                    battleState = PerformAction.WAIT;
                    return;
                }

                else if (i == (T2CharactersInBattle.Count - 1) && T2CharactersInBattle[i].GetComponent<P2StateMachine>().currentTurtleP2.hasAtck)
                {
                    Debug.Log("comprovan si puc pasar torn, si hauria");
                    passTurnObject.SetActive(true);
                    battleState = PerformAction.WAIT;
                }
            }
        }
    }

    public void passTorn()
    {
        if (turnPlayer1)
        {
            for (int i = 0; i < T1CharactersInBattle.Count; i++)
            {
                T1CharactersInBattle[i].GetComponent<P1StateMachine>().currentTurtleP1.hasAtck = false;
                if (T1CharactersInBattle[i].GetComponent<P1StateMachine>().statsModified && T1CharactersInBattle[i].GetComponent<P1StateMachine>().turnStatsModified != numOfTurn)
                {
                    T1CharactersInBattle[i].GetComponent<P1StateMachine>().ResetStats();
                }

            }
        }
        else 
        {
            for (int i = 0; i < T2CharactersInBattle.Count; i++)
            {
                T2CharactersInBattle[i].GetComponent<P2StateMachine>().currentTurtleP2.hasAtck = false;
                if (T2CharactersInBattle[i].GetComponent<P2StateMachine>().statsModified && T2CharactersInBattle[i].GetComponent<P2StateMachine>().turnStatsModified != numOfTurn)
                {
                    T2CharactersInBattle[i].GetComponent<P2StateMachine>().ResetStats();
                }
            }
        }
        turnPlayer1 = !turnPlayer1;
        if(turnPlayer1 == true)
        { 
            numOfTurn++;
            infoTurnPanel.GetComponent<Text>().text = numOfTurn.ToString();
        }
        passTurnObject.SetActive(false);
    }

    public void MoveInput()
    {
        turnInput = TurnGUI.INPUTMOVE;
        ActivateActionPanel = false;
        ActionPanel.SetActive(false);
    }

    public void InputDefense()
    {
        turnInput = TurnGUI.INPUT1;
        ActionPanel.SetActive(false);
        ActivateActionPanel = false;
        if (turnPlayer1)
        {
            currCharacter.GetComponent<P1StateMachine>().Defense();
        }
        else
        {
            currCharacter.GetComponent<P2StateMachine>().Defense();
        }

        turnInput = TurnGUI.DONE;
    }
}
