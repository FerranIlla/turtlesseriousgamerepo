﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class P1StateMachine : MonoBehaviour {

    private BattleStateMachine BSM;
    public TurtlePlayerStats currentTurtleP1;
    public GameObject t2CharacterToAttack;
    private bool actionStarted = false;
    public bool statsModified;
    public int turnStatsModified;
    public enum TurnState
    {
        NOTURN,//NO ESTAS EN EL TEU TORN
        WAITING, //IDDLE, esperant inputs
        ACTION,
        DEAD,

    }

    public TurnState currentState;

   /* public void changeStats(TurtlePlayerStats _t)
    {
        currentTurtleP1.theName = _t.theName;
        currentTurtleP1.baseAtk = _t.baseAtk;
        currentTurtleP1.baseHP = _t.baseHP;
        currentTurtleP1.baseDef = _t.baseDef;
        currentTurtleP1.currAtk = _t.currAtk;
        currentTurtleP1.currentHP = _t.currentHP;
        currentTurtleP1.currDef = _t.currDef;
        currentTurtleP1.hasAtck = _t.hasAtck;

    }*/
	void Start ()
    {
        currentState = TurnState.NOTURN;
        statsModified = false;
        BSM = GameObject.Find("BattleManager").GetComponent<BattleStateMachine>();
        checkDefense();
    }

	void Update () {

        switch (currentState)
        {
            case (TurnState.NOTURN):
                break;
            case (TurnState.WAITING):
                currentState = TurnState.WAITING;
                break;
            case (TurnState.ACTION):
                StartCoroutine(TimeForAction());
                break;
            case (TurnState.DEAD):
                break;
        }

        Die();
	}

    private IEnumerator TimeForAction()
    {
        if (actionStarted)
        {
            yield break;
        }

        actionStarted = true;
        Debug.Log("atacan");
        checkTypeAtk();
        yield return new WaitForSeconds(3);
        BSM.battleState = BattleStateMachine.PerformAction.ENDACTION;
        Debug.Log("ja he atacat");
        actionStarted = false;
        currentTurtleP1.hasAtck = true;
        currentState = TurnState.WAITING;
    }

    public List<GameObject> targetsInRangeAtk()
    {
        List<GameObject> aux = new List<GameObject>();
        foreach(GameObject possibleTarget in BSM.T2CharactersInBattle)
        {
            float d = Vector3.Distance(this.gameObject.transform.position, possibleTarget.transform.position);
            if(d <= currentTurtleP1.range)
            {
                aux.Add(possibleTarget);
            }
        }
        if (aux != null)
        {
            return aux;
        }
        else return null;
    }

    void checkTypeAtk()
    {
        float d = Vector3.Distance(this.gameObject.transform.position, BSM.turnChoise.TargetsGameObject.transform.position);

        //suposant que el rang de cos a cos Per tots es de 5:
        if (d <= 1) { } //atk cos a cos}
        else
        {
            distanceAtk();
        }
    }

    void distanceAtk()
    {
        currentTurtleP1.currentPrecision = currentTurtleP1.basePrecision; // + variables que influeixin en modificar la precisio
        int prob = Random.Range(0, 101);
        if(prob < currentTurtleP1.currentPrecision)
        {
            currentTurtleP1.currAtk = currentTurtleP1.baseAtk;//+ variables que influeixin en variar la potencia del atk
            float atk = currentTurtleP1.currAtk;
            float defTurtle = BSM.turnChoise.TargetsGameObject.GetComponent<P2StateMachine>().currentTurtleP2.currDef;
            float dmg = atk - defTurtle;
            BSM.turnChoise.TargetsGameObject.GetComponent<P2StateMachine>().GetDmg((int)dmg); 
        }
        else
        {
            Debug.Log("ui has fallat");
        }
    }

    void checkDefense()
    {
        currentTurtleP1.currDef = currentTurtleP1.baseDef;//+ variables
    }

    public void Defense()
    {
        this.currentTurtleP1.currDef += 20;
        currentTurtleP1.hasAtck = true;
        statsModified = true;
        turnStatsModified = BSM.numOfTurn;
        BSM.battleState = BattleStateMachine.PerformAction.ENDACTION;
        currentState = TurnState.WAITING;       
    }

    public void ResetStats()
    {
        this.currentTurtleP1.currAtk = currentTurtleP1.baseAtk;
        this.currentTurtleP1.currDef = currentTurtleP1.baseDef;
        this.currentTurtleP1.currentPrecision = currentTurtleP1.basePrecision;
        statsModified = false;
    }

    public void GetDmg(int dmg)
    {
        currentTurtleP1.currentHP += dmg;
    }

    void Die()
    {
        if (this.gameObject.transform.position.y < -5)
        {
            BSM.T1CharactersInBattle.Remove(this.gameObject);
            if (BSM.turnPlayer1)
            {
                if (BSM.currCharacter == this.gameObject) { BSM.currCharacter = null; }
                BSM.turnInput = BattleStateMachine.TurnGUI.WAITING;
                BSM.battleState = BattleStateMachine.PerformAction.ENDACTION;
                currentState = TurnState.DEAD;
            }
            BSM.numTP1.text = BSM.T1CharactersInBattle.Count.ToString();        
            Destroy(this.gameObject);
        }
    }
}
