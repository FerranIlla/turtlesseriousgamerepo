﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class P2StateMachine : MonoBehaviour
{ 
    private BattleStateMachine BSM;
    public TurtlePlayerStats currentTurtleP2;
    public GameObject t1CharacterToAttack;
    private bool actionStarted = false;
    public bool statsModified;
    public int turnStatsModified;
    public enum TurnState
    {
        NOTURN,//NO ESTAS EN EL TEU TORN
        WAITING, //IDDLE, esperant inputs
        ACTION,
        DEAD,

    }

    public TurnState currentState;

    void Start()
    {
        statsModified = false;
        currentState = TurnState.NOTURN;
        BSM = GameObject.Find("BattleManager").GetComponent<BattleStateMachine>();
        checkDefense();
    }

    void Update()
    {

        switch (currentState)
        {
            case (TurnState.NOTURN):
                break;
            case (TurnState.WAITING):
                //BSM.turnToManage.Add(this.gameObject);
                currentState = TurnState.WAITING;
                break;
            case (TurnState.ACTION):
                StartCoroutine(TimeForAction());
                break;
            case (TurnState.DEAD):
                break;
        }

        Die();
    }

    private IEnumerator TimeForAction()
    {
        if (actionStarted)
        {
            yield break;
        }

        actionStarted = true;
        Debug.Log("atacan");
        checkTypeAtk();
        yield return new WaitForSeconds(3);
        BSM.battleState = BattleStateMachine.PerformAction.ENDACTION;
        Debug.Log("ja he atacat");
        actionStarted = false;
        currentTurtleP2.hasAtck = true;
        currentState = TurnState.WAITING;
    }

    public List<GameObject> targetsInRangeAtk()
    {
        List<GameObject> aux = new List<GameObject>();
        foreach (GameObject possibleTarget in BSM.T1CharactersInBattle)
        {
            float d = Vector3.Distance(this.gameObject.transform.position, possibleTarget.transform.position);
            if (d <= currentTurtleP2.range)
            {
                aux.Add(possibleTarget);
            }
        }
        if (aux != null)
        {
            return aux;           
        }
        else return null;
    }

    void checkTypeAtk()
    {
        float d = Vector3.Distance(this.gameObject.transform.position, BSM.turnChoise.TargetsGameObject.transform.position);
        if(d <= 1) { }
        else
        {
            distanceAtk();
        }

    }

    void distanceAtk()
    {
        currentTurtleP2.currentPrecision = currentTurtleP2.basePrecision;
        int prob = Random.Range(0, 101);
        if (prob < currentTurtleP2.currentPrecision)
        {
            currentTurtleP2.currAtk = currentTurtleP2.baseAtk;
            float atk = currentTurtleP2.currAtk;
            float defTurtle = BSM.turnChoise.TargetsGameObject.GetComponent<P1StateMachine>().currentTurtleP1.currDef;
            float dmg = atk - defTurtle;
            BSM.turnChoise.TargetsGameObject.GetComponent<P1StateMachine>().GetDmg((int)dmg);
        }
        else
        {
            Debug.Log("ui has fallat");
        }
    }

    void checkDefense()
    {
        currentTurtleP2.currDef = currentTurtleP2.baseDef;
    }
    public void Defense()
    {
        currentTurtleP2.currDef += 20;
        currentTurtleP2.hasAtck = true;
        statsModified = true;
        turnStatsModified = BSM.numOfTurn;
        BSM.battleState = BattleStateMachine.PerformAction.ENDACTION;
        currentState = TurnState.WAITING;
    }

    public void ResetStats()
    {
        currentTurtleP2.currAtk = currentTurtleP2.baseAtk;
        currentTurtleP2.currDef = currentTurtleP2.baseDef;
        currentTurtleP2.currentPrecision = currentTurtleP2.basePrecision;
        statsModified = false;
    }

    public void GetDmg(int dmg)
    {
        currentTurtleP2.currentHP += dmg;
    }

    void Die()
    {
        if(this.gameObject.transform.position.y < -5)
        {
            BSM.T2CharactersInBattle.Remove(this.gameObject);
            if (!BSM.turnPlayer1)
            {
                if (BSM.currCharacter == this.gameObject) { BSM.currCharacter = null; }
                BSM.numTP2.text = BSM.T2CharactersInBattle.Count.ToString();
                BSM.turnInput = BattleStateMachine.TurnGUI.WAITING;
                BSM.battleState = BattleStateMachine.PerformAction.ENDACTION;
                currentState = TurnState.DEAD;
            }
;
            BSM.numTP2.text = BSM.T2CharactersInBattle.Count.ToString();
            Destroy(this.gameObject);

        }
    }
}
