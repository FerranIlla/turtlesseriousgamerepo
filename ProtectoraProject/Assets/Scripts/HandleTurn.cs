﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class HandleTurn {

    public string team;
    public string Attacker;//name of who attacks
    public GameObject AttackersGameObject;//who are sending information
    public GameObject TargetsGameObject;//who are going to be attacked

    //which attack is perform

}
