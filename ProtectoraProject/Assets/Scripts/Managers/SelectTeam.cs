﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectTeam : MonoBehaviour {

    public GameObject infoTurnPanel;
    public GameObject dynamicPanel;
    public GameObject list;
    public GameObject playButton;

    private bool turnP1;
    [SerializeField]
    private List<GameObject> turtles = new List<GameObject>();
    private int selectIndex = 0;

    private Text dynamicText;
    private WikiTurtles wikipedia = new WikiTurtles();

    private GameObject specieP1, specieP2;

    void Start ()
    {
        playButton.SetActive(false);
        foreach (Transform T in list.gameObject.transform) 
        {
            turtles.Add(T.gameObject);
            T.gameObject.SetActive(false);
        }
        turtles[selectIndex].SetActive(true);

        dynamicText = dynamicPanel.GetComponent<Text>();
        currentCharacter(0);
        list.SetActive(false);
        turnP1 = true;
	}

    public void currentCharacter(int index)
    {
        if(index < 0 || index >= turtles.Count) { return; }

        turtles[selectIndex].SetActive(false);
        turtles[index].SetActive(true);
        selectIndex = index;
        updateText(selectIndex);
    }

    public void updateText(int idx)
    {
        if(idx == 0)
        {
            dynamicText.text = wikipedia.Specie1;
        }
        else if (idx == 1)
        {
            dynamicText.text = wikipedia.Specie2;
        }
        else if (idx == 2)
        {
            dynamicText.text = wikipedia.Specie3;
        }
        else if (idx == 3)
        {
            dynamicText.text = wikipedia.Specie4;
        }
        else if (idx == 4)
        {
            dynamicText.text = wikipedia.Specie5;
        }
    }

    public void SelectTurtle()
    {
        if (turnP1)
        {
            specieP1 = Instantiate(turtles[selectIndex]);
            Destroy(specieP1.GetComponent<P2StateMachine>());
            specieP1.tag = "CharacterP1";
            specieP1.transform.localScale = new Vector3(1, 0.2f, 1);
            specieP1.transform.position = specieP1.transform.localPosition;
            list.SetActive(false);
            showInfoPanel();
            turnP1 = false;
        }
        else
        {
            specieP2 = Instantiate(turtles[selectIndex]);
            Destroy(specieP2.GetComponent<P1StateMachine>());
            specieP2.tag = "CharacterP2";
            specieP2.transform.localScale = new Vector3(1, 0.2f, 1);
            specieP2.transform.position = specieP2.transform.localPosition;
            list.SetActive(false);
            playButton.SetActive(true);
        }
        
        //per que el jugador no sapiga quin team ha escollit laltre
        turtles[selectIndex].SetActive(false);
        selectIndex = 0;
        updateText(selectIndex);
        turtles[selectIndex].SetActive(true);
    }
    public void loadPlayScene(string sceneToLoad)
    {
        if(specieP1 != null && specieP2 != null)
        {
           DontDestroyOnLoad(specieP1);
           DontDestroyOnLoad(specieP2);
           SceneManager.LoadScene(sceneToLoad);
        } 
    }

    void showInfoPanel()
    {
        string newText = "Player2 chooise your team";
        Text aux = infoTurnPanel.GetComponentInChildren<Text>();
        aux.text = newText;
        infoTurnPanel.SetActive(true);
    }

    public void closeShowInfoPanel()
    {
        infoTurnPanel.SetActive(false);
        list.SetActive(true);
        
    }
}
