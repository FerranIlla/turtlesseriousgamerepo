﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	//PARA QUE FUNCIONE BIEN PRIMERO PONER A TRUE FOLLOWTARGET Y LUEGO SEGUIDO ACTIVAR ZOOM IN o ZOOM OUT
	
	public float smooth = 5;
	public int zoomPerspective = 5;
	public int normalPerspective = 18;
	public GameObject target;
	public int zoomOrtographic = 3;
	public int normalOrtographic = 18;
	public bool zoomOut;
	public bool zoomIn;
	private int fieldOfView;
	private int sizeOrtograpghic;
	
	//atributs de moviment
	private Vector3 dirToTarget;
	public bool followTarget;
	public float smoothTime = 1.0f;
	private Vector3 velocity = Vector3.zero;
	
	// Use this for initialization
	void Start () {
		fieldOfView = (int)GetComponent<Camera>().fieldOfView;
	}
	
	// Update is called once per frame
	void Update () {


		if (target != null)
		{
			if (zoomIn && fieldOfView > zoomPerspective)
			{
				zoomOut = false;
				ZoomInTargetPerspective(target);
			}
			
			else if (zoomOut && sizeOrtograpghic < normalPerspective)
			{
				zoomIn = false;
				ZoomOutTargetPerspective(target);
			}
			
			if (followTarget)
			{
				FollowTarget(target);
			}
		}

	}
	
	#region grupDeFuncionsZoom
	public void ZoomInTargetPerspective(GameObject zoomObject)
	{
		transform.LookAt(zoomObject.transform);
		GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, zoomPerspective, Time.deltaTime * smooth);
		fieldOfView = (int)GetComponent<Camera>().fieldOfView;
	}

	public void ZoomOutTargetPerspective(GameObject zoomObject)
	{
		transform.LookAt(zoomObject.transform);
		GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, normalPerspective, Time.deltaTime * smooth);
		fieldOfView = (int)GetComponent<Camera>().fieldOfView;
	}
	
	
	public void ZoomInTargetOrtographic(GameObject zoomObject)
	{
		transform.LookAt(zoomObject.transform);
		GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, zoomOrtographic, Time.deltaTime * smooth);
		sizeOrtograpghic = (int)GetComponent<Camera>().orthographicSize;
	}

	public void ZoomOutTargetOrtographic(GameObject zoomObject)
	{
		transform.LookAt(zoomObject.transform);
		GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, normalOrtographic, Time.deltaTime * smooth);
		sizeOrtograpghic = (int)GetComponent<Camera>().orthographicSize;
	}
	
	#endregion grupDeFuncionsZoom
	
	#region movimentCamera

	public void FollowTarget(GameObject targetToFollow)
	{
		transform.rotation = Quaternion.Euler(90, 0, 0);
		dirToTarget = new Vector3(targetToFollow.transform.position.x, gameObject.transform.position.y, targetToFollow.transform.position.z);
		transform.position = Vector3.SmoothDamp(transform.position, dirToTarget, ref velocity, smoothTime);
		if (Vector3.Distance(dirToTarget, transform.position) <= 0.2f)
		{
			followTarget = false;
			
		}
		
	}
	
	#endregion movimentCamera
}
