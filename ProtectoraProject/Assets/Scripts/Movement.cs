﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    private BattleStateMachine BSM;
    private int length = 0;
    private bool initSW = false;

    private Vector3 final;
    private Vector3 _startpos;
    private Vector3 _endpos;
    private bool isTurtle;
    public int limitMaxLength;
    private GameObject selectedTurtle;
    private GameObject barPower;
 

    private float angleActualDirection = 0;
    private bool isMovement;
    private void Start()
    {
        isMovement = false;
        BSM = GameObject.FindObjectOfType<BattleStateMachine>();
    }
    void Update()
    {
        if(this.gameObject.GetComponent<Rigidbody>().velocity == new Vector3(0, 0, 0) && isMovement) { endMovement(); }
#if UNITY_EDITOR
        
        if (Input.GetMouseButtonDown(0))
        {
            
            Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                if (raycastHit.collider.gameObject == BSM.currCharacter)
                {
                    selectedTurtle = raycastHit.collider.gameObject;
                    selectedTurtle.transform.GetChild(0).gameObject.SetActive(true);
                    barPower = selectedTurtle.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
                    barPower.GetComponent<EnergyBar>().SetValueCurrent(0);

                    if (BSM.turnPlayer1)
                    {
                        selectedTurtle.GetComponent<P1StateMachine>().currentTurtleP1.arrowAngleDirection = 0;
                    } else
                    {
                        selectedTurtle.GetComponent<P2StateMachine>().currentTurtleP2.arrowAngleDirection = 0;
                    }
                    
                    isTurtle = true;
                 //   selectedTurtle.GetComponent<Renºrer>().material.color = new Color(1f, 0.12f, 0.13f);
                    final = Vector3.zero;
                    length = 0;
                    
                    Vector2 touchDeltaPosition = Input.mousePosition;
                    _startpos = new Vector3(-touchDeltaPosition.x, 0, -touchDeltaPosition.y);
                    initSW = true;
                    
                }
            }
        }

        else if (isTurtle && Input.GetMouseButton(0) && initSW)
        {
           
            Vector2 touchPosition = Input.mousePosition;
            _endpos = new Vector3(-touchPosition.x, 0, -touchPosition.y);
            final = _endpos - _startpos;
            length = (int)final.magnitude;
            barPower.GetComponent<EnergyBar>().SetValueCurrent(length);
        
            if (length >= limitMaxLength)
            {
                length = limitMaxLength;
                barPower.GetComponent<EnergyBar>().SetValueCurrent(length);
            }
        
       
            else if(length > limitMaxLength && length < limitMaxLength)
            {
                length = (int)final.magnitude;
                barPower.GetComponent<EnergyBar>().SetValueCurrent(length);
            }

            if (BSM.turnPlayer1)
            {
                selectedTurtle.GetComponent<P1StateMachine>().currentTurtleP1.arrowAngleDirection = Mathf.Atan2(-final.x, -final.z) * Mathf.Rad2Deg;
                if (!float.IsNaN(selectedTurtle.GetComponent<P1StateMachine>().currentTurtleP1.arrowAngleDirection))
                {
                    selectedTurtle.transform.GetChild(0).RotateAround(
                        selectedTurtle.gameObject.transform.position, selectedTurtle.gameObject.transform.up,
                        selectedTurtle.GetComponent<P1StateMachine>().currentTurtleP1.arrowAngleDirection -
                        selectedTurtle.GetComponent<P1StateMachine>().currentTurtleP1.arrowActualAngleDirection);
                    selectedTurtle.GetComponent<P1StateMachine>().currentTurtleP1.arrowActualAngleDirection = selectedTurtle.GetComponent<P1StateMachine>().currentTurtleP1.arrowAngleDirection;
                }
            }
            else
            {
                selectedTurtle.GetComponent<P2StateMachine>().currentTurtleP2.arrowAngleDirection = Mathf.Atan2(-final.x, -final.z) * Mathf.Rad2Deg;

                if (!float.IsNaN(selectedTurtle.GetComponent<P2StateMachine>().currentTurtleP2.arrowAngleDirection))
                {
                    selectedTurtle.transform.GetChild(0).RotateAround(selectedTurtle.transform.position, selectedTurtle.transform.up,
                        selectedTurtle.GetComponent<P2StateMachine>().currentTurtleP2.arrowAngleDirection - 
                        selectedTurtle.GetComponent<P2StateMachine>().currentTurtleP2.arrowActualAngleDirection);
                    selectedTurtle.GetComponent<P2StateMachine>().currentTurtleP2.arrowActualAngleDirection = selectedTurtle.GetComponent<P2StateMachine>().currentTurtleP2.arrowAngleDirection;
                }
            }

            
            
            
        }

        else if (isTurtle && Input.GetMouseButtonUp(0) && initSW)
        {
           // selectedTurtle.GetComponent<Renderer>().material.color = new Color(0.1f, 1f, 0.07f);
        
        
            barPower.GetComponent<EnergyBar>().SetValueCurrent(0);
            selectedTurtle.transform.GetChild(0).gameObject.SetActive(false);
            
            selectedTurtle.GetComponent<Rigidbody>().AddForce(-final.normalized * length, ForceMode.Impulse);
            
            
            initSW = false;
            isTurtle = false;
            isMovement = true;
            
            
        }      
#endif

#if UNITY_ANDROID
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit raycastHit;
                if (Physics.Raycast(raycast, out raycastHit))
                {
                    if (raycastHit.collider.CompareTag("Turtle"))
                    {
                        selectedTurtle = raycastHit.collider.gameObject;
                        selectedTurtle.transform.GetChild(0).gameObject.SetActive(true);
                        barPower = selectedTurtle.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
                        barPower.GetComponent<EnergyBar>().SetValueCurrent(0);
                    
                        selectedTurtle.GetComponent<Turtle>().arrowAngleDirection = 0;
                        //angleActualDirection = 0;
                        
                        isTurtle = true;
                     //   selectedTurtle.GetComponent<Renderer>().material.color = new Color(1f, 0.12f, 0.13f);
                        final = Vector3.zero;
                        length = 0;
                        
                        Vector2 touchDeltaPosition = Input.mousePosition;
                        _startpos = new Vector3(-touchDeltaPosition.x, 0, -touchDeltaPosition.y);
                        initSW = true;
                    }
                }


            }
            else if (Input.touchCount > 0 && isTurtle && initSW && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Vector2 touchPosition = Input.GetTouch(0).position;
                _endpos = new Vector3(-touchPosition.x, 0, -touchPosition.y);
                final = _endpos - _startpos;
                length = (int)final.magnitude;
                barPower.GetComponent<EnergyBar>().SetValueCurrent(length);
            
                if (length >= limitMaxLength)
                {
                    length = limitMaxLength;
                    barPower.GetComponent<EnergyBar>().SetValueCurrent(length);
                }
            
           
                else if(length > limitMaxLength && length < limitMaxLength)
                {
                    length = (int)final.magnitude;
                    barPower.GetComponent<EnergyBar>().SetValueCurrent(length);
                }
    
                selectedTurtle.GetComponent<Turtle>().arrowAngleDirection = Mathf.Atan2(final.x, final.z) * Mathf.Rad2Deg;
    
                if (!float.IsNaN(selectedTurtle.GetComponent<Turtle>().arrowAngleDirection))
                {
                    selectedTurtle.transform.GetChild(0).RotateAround(selectedTurtle.transform.position, selectedTurtle.transform.up, selectedTurtle.GetComponent<Turtle>().arrowAngleDirection - selectedTurtle.GetComponent<Turtle>().arrowActualAngleDirection);
                    selectedTurtle.GetComponent<Turtle>().arrowActualAngleDirection = selectedTurtle.GetComponent<Turtle>().arrowAngleDirection;
    
                }
            }

            /* 
            else if (Input.touchCount > 0 && isTurtle && Input.GetTouch(0).phase == TouchPhase.Canceled)
            {
                SW = false;
            }

            else if (Input.touchCount > 0 && isTurtle && Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                SW = false;
            }
            */
            else if (Input.touchCount > 0 && isTurtle && initSW && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
               // selectedTurtle.GetComponent<Renderer>().material.color = new Color(0.1f, 1f, 0.07f);
            
            
                barPower.GetComponent<EnergyBar>().SetValueCurrent(0);
                selectedTurtle.transform.GetChild(0).gameObject.SetActive(false);
                
                selectedTurtle.GetComponent<Rigidbody>().AddForce(final.normalized * length, ForceMode.Impulse);
                
                
                initSW = false;
                isTurtle = false;
            }
#endif

    }

    public void endMovement()
    {
       // BSM.currCharacter = null;
        BSM.turnInput = BattleStateMachine.TurnGUI.DONE;
        BSM.battleState = BattleStateMachine.PerformAction.ENDACTION;
        if (BSM.turnPlayer1)
        {
            selectedTurtle.GetComponent<P1StateMachine>().currentTurtleP1.hasAtck = true;

        }
        else
        {
            selectedTurtle.GetComponent<P2StateMachine>().currentTurtleP2.hasAtck = true;
        }

        this.gameObject.GetComponent<Movement>().enabled = false;
    }

}
