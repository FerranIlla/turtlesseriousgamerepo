﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class TurtlePlayerStats {


    public string theName;

    public float baseHP;
    public float currentHP;

    public float baseAtk;
    public float currAtk;

    public float baseDef;
    public float currDef;

    public float range;
    public float basePrecision;
    public float currentPrecision;
    public bool hasAtck;

    //var MOviment
    public float arrowAngleDirection;
    public float arrowActualAngleDirection;

    public enum Type
    {
        SPECIE1, SPECIE2,
    }

    /*public TurtlePlayerStats(string _specie)
    {
        if(_specie == Type.SPECIE1.ToString())
        {
            theName = "TortugaSpecie1";
            range = 10;
            baseAtk = 5;
            baseDef = 4;
        }
        if (_specie == Type.SPECIE2.ToString())
        {
            theName = "TortugaSpecie2";
            range = 20;
            baseAtk = 2;
            baseDef = 2;
        }

        baseHP = 0;
        currentHP = 0;
        currAtk = 0;
        currDef = 0;
        hasAtck = false;
    }*/


}
